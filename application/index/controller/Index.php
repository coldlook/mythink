<?php
namespace app\index\controller;

use app\index\controller\HomeBase;
use think\Db;

class Index extends HomeBase
{
    public function index()
    {
        return $this->fetch();
    }
}
